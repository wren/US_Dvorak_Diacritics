# Dvorak with Diarcitics

This layout was made using [Microsoft Keyboard Layout Creator](https://www.microsoft.com/en-us/download/details.aspx?id=102134).

## How it Works

First you type the letter you want `e`, then the desired diacritic `ê`. 
Backspace deletes the diacritic before the letter, so if you typed the 
wrong letter you need to hit backspace twice.  

To enter the diacritic hold alt-greater and the corresponding key. 
Alt-greater can be used by holding the right-alt key or by holding 
ctrl+left-alt.

### Base Layout

![](./LayoutImages/Neutral.png)


### Alt Greater

![](./LayoutImages/AltGr.png)

### Shift + Alt Greater

![](./LayoutImages/Shift+AltGr.png)